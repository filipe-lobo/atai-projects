##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 02 (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question2.py
##################################################

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import quad

def f(y, x): 
	if 0 < y and y < x and x < 1:
		return 6 * y
	else:
		return 0

x = y = np.linspace(0, 1, 1000)
X, Y = np.meshgrid(x, y)
zs = np.array([f(y, x) for x,y in zip(np.ravel(X), np.ravel(Y))])
Z = zs.reshape(X.shape)

# Joint probability plot
fig = plt.figure()
ax  = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, Z, cmap=cm.coolwarm, linewidth=0)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('fXY(x,y)')
plt.show()

def divide0(x, y):
	if y == 0:
		return 0
	else:
		return x / y

# Marginal probability plot
marginalpdf = []
for i in x:
	value, _ = quad(f, 0, 1, args=(i))
	marginalpdf.append(value)
print(len(marginalpdf))
plt.plot(x, marginalpdf, 'b', label='fX(x)')
plt.xlabel('X')
plt.legend()
plt.show()

# Conditional probability plot
conditionalpdf = np.array([[divide0(f(yValue, xValue), marginalpdf[idx]) for _, xValue in enumerate(x)] for idx, yValue in enumerate(y)])
C = conditionalpdf.reshape(X.shape)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, C, cmap=cm.coolwarm, linewidth=0)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('fY|X(y|x)')
plt.show()

# Expectation plot
expectation = np.array([[i*x[idx] for i in line] for idx, line in enumerate(C)])
E = expectation.reshape(X.shape)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot_surface(X, Y, E, cmap=cm.coolwarm, linewidth=0)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('E(Y|x)')
plt.show()

