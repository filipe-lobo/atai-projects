##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 02 (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question1.py
##################################################

import collections
import math
import pylab
import numpy as np

alphabet = [' ', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'w', 'y', 'z']

def readTextFile(textFile):
	with open(textFile) as f:
		result = f.readlines()
	for index, line in enumerate(result):
		newline = [x.lower() for x in line if x.lower() in alphabet]
		result[index] = newline
	return result

def probDistribution(textArray):
	probDict = collections.OrderedDict()
	textSize = sum([len(line) for line in textArray])
	for letter in alphabet:
		summation = sum([len(char) for line in textArray for char in line if char == letter])
		probDict[letter] = summation / textSize
	return probDict

def jointProbDistribution(textArray):	
	jointProbDict = collections.OrderedDict()
	for x in alphabet:
		jointProbDict[x] = collections.OrderedDict()
		for y in alphabet:
			jointProbDict[x][y] = 0
	digrams = 0
	for line in textArray:
		for index, x in enumerate(line):
			try:
				y = line[index + 1]
				digrams += 1
				jointProbDict[x][y] += 1
			except IndexError:
				pass
	for x, values in jointProbDict.items():
		for y, value in values.items():
			jointProbDict[x][y] = value / digrams
	return jointProbDict

def conditionalProbDistribution(probDict, jointProbDict):
	conditionalXY, conditionalYX = collections.OrderedDict(), collections.OrderedDict()
	for letter in alphabet:
		conditionalXY[letter], conditionalYX[letter] = collections.OrderedDict(), collections.OrderedDict()
	for x in alphabet:
		for y in alphabet:
			conditionalXY[x][y], conditionalYX[x][y] = (jointProbDict[x][y] / probDict[y]), (jointProbDict[x][y] / probDict[x])
	return conditionalXY, conditionalYX

def shannonInformationContent(probDict):
	shannon = collections.OrderedDict()
	for letter in alphabet:
		shannon[letter] = -math.log(probDict[letter], 2)
	return shannon

def entropyCalc(probDict):
	entropy = 0
	for letter in alphabet:
		entropy += probDict[letter] * (-math.log(probDict[letter], 2))
	return entropy

def kullbackLeiblerDivergence(documentsResults):
	divergenceMatrix = collections.OrderedDict()
	documentKeys = documentsResults.keys()
	for key in documentKeys:
		divergenceMatrix[key] = collections.OrderedDict()
	for i in documentKeys:
		for j in documentKeys:
			divergenceMatrix[i][j] = sum([documentsResults[i][1][x]*math.log(documentsResults[i][1][x]/documentsResults[j][1][x]) for x in alphabet])
	return divergenceMatrix

documents = ['brazilian_tales_2017_1/LIFE_by_JM_Machado_de_Assis.txt', 'brazilian_tales_2017_1/THE_ATTENDANT_S_CONFESSION_by_JM_Machado_de_Assis.txt', 'brazilian_tales_2017_1/THE_FORTUNE_TELLER_by_JM_Machado_de_Assis.txt', 'brazilian_tales_2017_1/THE_PIGEONS_by_Coelho_Netto.txt', 'brazilian_tales_2017_1/THE_VENGEANCE_OF_FELIX_by_J_de_Medeiros_E_Albuquerque.txt']

resultsDict = collections.OrderedDict()
for document in documents:
	result = readTextFile(document)
	probDict = probDistribution(result)
	jointProbDict = jointProbDistribution(result)
	conditionalXY, conditionalYX = conditionalProbDistribution(probDict, jointProbDict)
	shannon = shannonInformationContent(probDict)
	entropy = entropyCalc(probDict)
	resultsDict[document] = [result, probDict, jointProbDict, conditionalXY, conditionalYX, shannon, entropy]
divergenceMatrix = kullbackLeiblerDivergence(resultsDict)

for document in resultsDict:
	name = resultsDict[document][5]
	print(document)
	print([(x, round(name[x], 9)) for x in name])

def blob(x, y, area, color):
	hs = np.sqrt(area) / 2
	xcorners = np.array([x - hs, x + hs, x + hs, x - hs])
	ycorners = np.array([y - hs, y - hs, y + hs, y + hs])
	pylab.fill(xcorners, ycorners, color, edgecolor=color)

def hinton(w, maxWeight=None):
	reenable = False
	if pylab.isinteractive():
		pylab.ioff()
	pylab.clf()
	(height, width) = w.shape
	if height is None:
		height = 1
	if width is None:
		width = 1
	if not maxWeight:
		maxWeight = 2**np.ceil(np.log(np.max(np.abs(w)))/np.log(2))

	pylab.fill(np.array([0, width, width, 0]), np.array([0, 0, height, height]), 'black')
	pylab.axis('off')
	pylab.axis('equal')
	for x in iter(range(width)):
		for y in iter(range(height)):
			_x = x + 1
			_y = y + 1
			W = w[y,x]
			if W > 0:
				blob(_x - .5, height - _y + .5, min(1, W/maxWeight), 'red')
			elif W < 0:
				blob(_x - .5, height - _y + .5, min(1, -W/maxWeight), 'pink')
	if reenable:
		pylab.ion()
	pylab.show()
"""
# PROBABILITY P(x) PLOT
for document in resultsDict:
	hinton(np.array(list(resultsDict[document][1].values()))[:, np.newaxis])

# JOINT PROBABILITY P(x, y) PLOT
jointHintonMatrix, idx = [], 0
for document in resultsDict:
	jointProbDict = resultsDict[document][2]
	for letter in jointProbDict:
		jointHintonMatrix.append(list(jointProbDict[letter].values()))
		idx += 1
	hinton(np.array(jointHintonMatrix))
	jointHintonMatrix, idx = [], 0

# CONDITIONAL PROBABILITY P(x | y) PLOT
conditionalXYHintonMatrix, idx = [], 0
for document in resultsDict:
	conditionalXY = resultsDict[document][3]
	for letter in conditionalXY:
		conditionalXYHintonMatrix.append(list(conditionalXY[letter].values()))
		idx += 1
	hinton(np.array(conditionalXYHintonMatrix))
	conditionalXYHintonMatrix, idx = [], 0

# CONDITIONAL PROBABILITY P(y | x) PLOT
conditionalYXHintonMatrix, idx = [], 0
for document in resultsDict:
	conditionalYX = resultsDict[document][4]
	for letter in conditionalYX:
		conditionalYXHintonMatrix.append(list(conditionalYX[letter].values()))
		idx += 1
	hinton(np.array(conditionalYXHintonMatrix))
	conditionalYXHintonMatrix, idx = [], 0

# SHANNON PROPERTY h(x) PLOT
for document in resultsDict:
	hinton(np.array(list(resultsDict[document][5].values()))[:, np.newaxis])

# ENTROPY VALUES
for document in resultsDict:
	print(resultsDict[document][6])

# DIVERGENCE MATRIX
for item in divergenceMatrix.items():
	print(item[0])
	print(list(item[1].values()))
"""
