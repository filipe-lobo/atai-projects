%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[11pt]{article}

\usepackage[margin=1in]{geometry} 
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{bm}
\usepackage{graphicx}
\usepackage{float}
\usepackage{color}

\newenvironment{EX}[2][Exercise]{\begin{trivlist}
\item[{\color{red} \hskip \labelsep {\bfseries #1}\hskip \labelsep {\bfseries #2.}}]}{\end{trivlist}}

\newenvironment{SL}[1][Solution]{\begin{trivlist}
\item[{\color{blue} \hskip \labelsep {\bfseries #1:}}]}{\end{trivlist}}


\begin{document}

% --------------------------------------------------------------
% Start here
% --------------------------------------------------------------

\noindent 344066 (Joaquim Filipe Lobo de Sousa) \hfill {\Large \bfseries CK0146: Exercise 04 (2017.1)} \\

\begin{EX}{Q.1}\
\end{EX}

\begin{SL}\
For this exercise we need to estimate the parameter vector \(w\) given a collection of data \(\{(x_i,t_i)\}\) with \(i=1,...,N\) from the file \verb|bigdata_xy_2017_1.txt| where \(N=8\). To estimate the parameters we need, first we will use the data, that was given in centimeters, in meters. So we have this collection of data below:
\[\{(0.44003316, 1.6793424),(0.49409332, 1.8117085),(0.46469245, 1.7313197),\]
\[(0.49400011, 1.8045534),(0.46053899, 1.7253482),(0.44235629, 1.6798254),\]
\[(0.51994983, 1.8615658),(0.41867176, 1.6297074)\}\] 

\begin{itemize}

\item \textbf{The maximum likelihood estimate,} \pmb{\(w_{ML}\)} \\
To estimate the maximum likelihood, we will need the formula below:
\[w_{ML}=(\Phi^T\Phi)^{-1}\Phi^Tt\]
where \(\Phi\) is the \(N \times M\) design matrix, with elements \(\phi_{nj} = \phi_j(x_n)\). Each \(\phi_j(x_n)\) is a basis function of the data \(x_n\). For this example we will need only two basis functions: the dummy function \(\phi_0(x) = 1\) and the function \(\phi_1(x)\) (since our function only have two parameters \(w\)). For simplicity we will take \(\phi_1(x) = x\), where \(x\) is the only value of the data entry. \\
So, doing the necessary calculations we arrive at the value:
\[w_{ML} = (0.64781587, 2.34066887)\]
Thus, if a person has a femur of length \(48cm\) his height would be (based on the \(w_{ML}\) values):
\[y(x,w) = w_0 + w_1x = 0.64781587 + 2.34066887 * 0.48 = 1.77 m\]

\item \textbf{The batch Bayesian estimate, the posterior} \pmb{\(p(w|t)\)} \\
To perform the batch estimate of the parameters \(w\) we will take all the data gathered from the file to construct the design matrix \(\Phi\) needed. The posterior distribution can be thus written directly in the form
\[p(w|t)=\mathcal{N}(w|m_N,S_N)\]
\[m_N=S_N(S_0^{-1}m_0+\beta\Phi^Tt)\]
\[S_N^{-1}=S_0^{-1}-\beta\Phi^T\Phi\]
Starting from a Gaussian prior \(w \sim \mathcal{N}(m_0,S_0=\alpha^{-1}I) \). Considering a simple form of the Gaussian distribution characterized by a single precision parameter \(\alpha\) we have the prior
\[p(w|\alpha)=\mathcal{N}(w|0,\alpha^{-1}I)\]
If we consider this infinitely broad prior \(S_0=\alpha^{-1}I\) with \(\alpha\rightarrow0\), the mean \(m_N\) of the posterior distribution reduces to the maximum likelihood value. Knowing that the noise added when measuring the heights \(y\) is Gaussian with zero mean and known standard deviation (\(\varepsilon \sim \mathcal{N}(\mu=0,\sigma=0.5)\)). So, with the values of \(\alpha=0.1\) and \(\beta=(\frac{1}{0.5})^2=4\), we will end up with the following value for \(w\) (\(w=m_N\)):
\[w = (1.26336172, 1.01353845)\]
So, if a person has a femur of length \(48cm\) his height would be:
\[y(x,w) = w_0 + w_1x = 1.26336172 + 1.01353845 * 0.48 = 1.75 m\]
Below, we have the plotting of the prior \(w\) distribution, the data space with the points used in the construction of the posterior, and the posterior distribution of \(w\):
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{batch}
\end{figure}

\item \textbf{The sequential Bayesian estimate, the posterior} \pmb{\(p(w|t)\)} \\
The sequential Bayesian estimate of \(w\) is calculated in the same manner of the batch, using the same formulas:
\[p(w|t)=\mathcal{N}(w|m_N,S_N)\]
\[m_N=S_N(S_0^{-1}m_0+\beta\Phi^Tt)\]
\[S_N^{-1}=S_0^{-1}-\beta\Phi^T\Phi\]
with the difference that we will use each subsequent data entry, one after the other, to calculate new posteriors. This means that, until the last data entry, we will use the last posterior value of \(w\) as the new prior to calculate the new posterior distribution. So, that being said, and using the same values as the batch calculation for the \(p(w|\alpha)=\mathcal{N}(w|0,\alpha^{-1}I)\) prior, we will end up with the same value, at the end, for the \(w\) parameters:
\[w = (1.26336172, 1.01353845)\]
So, if a person has a femur of length \(48cm\) his height would be:
\[y(x,w) = w_0 + w_1x = 1.26336172 + 1.01353845 * 0.48 = 1.75 m\]
Having the difference in the sequential estimate of taking new priors and posteriors in its iterations, we can plot the prior, posterior and the data space (as in the batch estimate) to observe the behavior of the Bayesian regression. Thus, we have below the iterations of the calculations, each accompanied by its posterior \(w\) value:

\begin{itemize}

\item Iteration 1: Posterior \(w= (1.37805858, 0.60639147)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential0}
\end{figure}

\item Iteration 2: Posterior \(w= (1.380577, 0.74442005)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential1}
\end{figure}

\item Iteration 3: Posterior \(w= (1.38201026, 0.74476371)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential2}
\end{figure}

\item Iteration 4: Posterior \(w= (1.37688164, 0.7845311)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential3}
\end{figure}

\item Iteration 5: Posterior \(w= (1.37289267, 0.78759225)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential4}
\end{figure}

\item Iteration 6: Posterior \(w= (1.35347323, 0.81461163)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential5}
\end{figure}

\item Iteration 7: Posterior \(w= (1.31223306, 0.9274842)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential6}
\end{figure}

\item Iteration 8: Posterior \(w= (1.26336172, 1.01353845)\)
\begin{figure}[H]
\centering
\includegraphics[scale=0.25]{sequential7}
\end{figure}

\end{itemize}

\end{itemize}

\end{SL}

% --------------------------------------------------------------
% Stop here
% --------------------------------------------------------------

\end{document}