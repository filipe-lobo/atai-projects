##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 04 (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question1.py
##################################################

import numpy as np
from numpy import linalg as la
import matplotlib.pyplot as plt
from numpy.random import multivariate_normal

class BayesianLinearRegression:
	def __init__(self, alpha=1., m0=0, sigmaError=.5):
		self.invAlpha = 1./float(alpha)
		self.m0 = float(m0)
		self.sigmaError = sigmaError
	
	def addIntercept(self, X):
		newX = np.ones((X.shape[0],X.shape[1]+1), dtype=X.dtype)
		newX[:,1:] = X[:,:]
		return newX

	def MLE(self, X, T):
		X = self.addIntercept(X)
		self.omega = la.inv(X.T.dot(X)).dot(X.T.dot(T))
		return self.omega

	def batchMAP(self, X, T):
		omegaML = self.MLE(X, T)
		x = X
		X = self.addIntercept(X)
		
		fDim = X.shape[1]
		m0 = np.repeat(self.m0,fDim)
		S0 = np.diagflat(np.repeat(self.invAlpha,fDim))
		
		beta = (1. / self.sigmaError) ** 2
		Sn = la.inv(la.inv(S0) + (beta * X.T.dot(X)))
		mn = Sn.dot((beta * X.T.dot(T)) + (la.inv(S0).dot(m0)))
		self.omega = mn
		self.plotPriorDataSpaceAndPosterior(x, T, m0, S0, mn, Sn, omegaML, 'batch.png')
		return mn, Sn

	def sequentialMAP(self, X, T):
		first = True
		sequentialX = np.array([[x] for x in X])
		sequentialT = np.array([[t] for t in T])
		omegas = []
		sigmas = []
		for idx, x in enumerate(sequentialX):
			x = self.addIntercept(x)
			if first:
				fDim = x.shape[1]
				m0 = np.repeat(self.m0,fDim)
				S0 = np.diagflat(np.repeat(self.invAlpha,fDim))
				first = False
			beta = (1. / self.sigmaError) ** 2
			Sn = la.inv(la.inv(S0) + (beta * x.T.dot(x)))
			mn = Sn.dot((beta * x.T.dot(sequentialT[idx])) + (la.inv(S0).dot(m0)))
			self.omega = mn
			omegas.append(mn)
			sigmas.append(Sn)

			DataXs = X[:idx+1]
			DataTs = T[:idx+1]
			omegaML = self.MLE(DataXs, DataTs)
			filename = 'sequential' + str(idx) + '.png'
			self.plotPriorDataSpaceAndPosterior(DataXs, DataTs, m0, S0, mn, Sn, omegaML, filename)

			S0 = Sn
			m0 = mn
		return np.array(omegas), np.array(sigmas)

	def predict(self, X):
		X = self.addIntercept(X)
		return X.dot(self.omega)

	def plotPriorDataSpaceAndPosterior(self, X, T, omegaPrior, sigmaPrior, omegaPosterior, sigmaPosterior, omegaML, filename):
		fig = plt.figure(figsize=(30,8))
		ax = plt.subplot(1, 3, 1)
		delta = 0.1
		x = np.arange(-5., 5., delta)
		y = np.arange(-5., 5., delta)
		z = np.zeros((len(x), len(y)))
		xx, yy = np.meshgrid(x, y)
		q = np.log(np.power(la.det(2 * np.pi * sigmaPrior), -0.5))
		for i in range(len(x)):
			for j in range(len(y)):
				beta = np.array([xx[i,j], yy[i,j]])
				d = beta - omegaPrior
				r = 1
				r += -0.5 * d.dot(la.inv(sigmaPrior)).dot(d[:,np.newaxis])
				z[i,j] = r
		plt.pcolor(xx, yy, z, vmin=0., vmax=1.)
		plt.scatter(omegaPrior[0], omegaPrior[1], c='k')
		plt.xlabel("w0")
		plt.ylabel("w1")
		plt.xlim(-5., 5.)
		plt.ylim(-5., 5.)
		plt.title("Omega Prior")

		ax = plt.subplot(1, 3, 2)
		ax.set_xlim(.4, .6)
		ax.set_ylim(1.5, 1.9)
		for i in range(0, 8):
			omega = multivariate_normal(omegaPosterior, sigmaPosterior)
			ax.plot([0, 1.0], [omega[0], omega[0] + 1.0 * omega[1]], color='k', linestyle='-', linewidth=1, zorder=1, alpha=0.3)
		ax.plot([0, 1.0], [omegaML[0], omegaML[0] + 1.0 * omegaML[1]], linestyle='-', linewidth=2, label="ML", zorder=100)
		ax.plot([0, 1.0], [omegaPosterior[0], omegaPosterior[0] + 1.0 * omegaPosterior[1]], linestyle='-', linewidth=2, label="MAP", zorder=100)
		ax.scatter(X, T, marker="o", color='r', edgecolor="black", s=100, zorder=10)
		plt.legend()
		plt.title("Data Space")

		ax = plt.subplot(1, 3, 3)
		delta = 0.1
		x = np.arange(-5., 5., delta)
		y = np.arange(-5., 5., delta)
		z = np.zeros((len(x), len(y)))
		xx, yy = np.meshgrid(x, y)
		q = np.log(np.power(la.det(2 * np.pi * sigmaPosterior), -0.5))
		for i in range(len(x)):
			for j in range(len(y)):
				beta = np.array([xx[i,j], yy[i,j]])
				d = beta - omegaPosterior
				r = 1
				r += -0.5 * d.dot(la.inv(sigmaPosterior)).dot(d[:,np.newaxis])
				z[i,j] = r
		plt.pcolor(xx, yy, z, vmin=0., vmax=1.)
		plt.scatter(omegaPosterior[0], omegaPosterior[1], c='k')
		plt.xlabel("w0")
		plt.ylabel("w1")
		plt.xlim(-5., 5.)
		plt.ylim(-5., 5.)
		plt.title("Omega Posterior")

		fig.savefig(filename)

filename = 'bigdata_xy_2017_1.txt'
data = []
with open(filename) as f:
	for line in f:
		data.append(list(map(float, line.split())))
data = np.array(data)

X = np.array([[x/100] for x in data[:, 0]])
T = np.array([t/100 for t in data[:, 1]])

clf = BayesianLinearRegression(alpha=.1, m0=0, sigmaError=.5)

# MLE
print('MLE:')
omegaML = clf.MLE(X, T)
print(omegaML)

# Batch
print('Batch:')
batchOmega, batchSigma = clf.batchMAP(X, T)
print(batchOmega)

# Sequential
print('Sequential:')
sequentialOmegas, sequentialSigmas = clf.sequentialMAP(X, T)
print(sequentialOmegas)
