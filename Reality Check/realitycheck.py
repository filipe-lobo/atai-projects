# realitycheck.py
# Author: Joaquim Filipe Lobo de Sousa

import numpy as np
import plotly.offline as py # plotly: The library used for plotting
import plotly.graph_objs as go

N = 4000 # The number of rows

# Reading the values from the file and saving in an array
f = open('reality_check_2017_1.txt', 'r') # Local file
l = [list(map(float, line.split())) for line in f]
l = np.array(l).transpose() # Tranposing to calculate the variance of the variables

# a: Variable to maintain a list of the variances
a = []
for lst in l:
	a.append(np.var(lst))

# The index of the two lists with the biggest variance
max1 = a.index(max(a))
a[max1] = 0
max2 = a.index(max(a))

# Variables for the plotting
x = np.linspace(0, 1, N)
y1 = l[max1]
y2 = l[max2]

trace1 = go.Scatter(
	x = x,
	y = y1,
	mode = 'markers',
	name = 'Largest Variance 01'
)

trace2 = go.Scatter(
	x = x,
	y = y2,
	mode = 'markers',
	name = 'Largest Variance 02'
)

data = [trace1, trace2]
obj = py.plot(data)
