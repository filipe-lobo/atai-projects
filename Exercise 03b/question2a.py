##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 03b (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question2a.py
##################################################

import numpy as np
from sklearn.mixture import GaussianMixture
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sklearn import mixture

points = []
file = 'unknown_xy_data_2017_1.txt'
with open(file) as f:
	for line in f:
		split = list(map(float, line.split()))
		points.append(split)
points = np.array(points)

clf = GaussianMixture(n_components=6, covariance_type='full')
clf.fit(points)

x = np.linspace(-6., 6., 50)
y = np.linspace(-6., 6., 50)
X, Y = np.meshgrid(x, y)
XX = np.array([X.ravel(), Y.ravel()]).T
Z = -clf.score_samples(XX)
Z = Z.reshape(X.shape)

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=plt.get_cmap('coolwarm'),
			linewidth=0, antialiased=True, vmax=18.)
fig.colorbar(surf, shrink=0.5, aspect=5) 
plt.savefig('3dgmm.png')
plt.clf()

CS = plt.contour(X, Y, Z, norm=LogNorm(vmin=1., vmax=18.), levels=np.logspace(0, 3, 10))
CB = plt.colorbar(CS, shrink=0.8, extend='both')
plt.scatter(points[:, 0], points[:, 1], s=.8)

plt.axis('tight')
plt.savefig('contourgmm.png')
plt.clf()
