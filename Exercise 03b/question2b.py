##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 03b (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question2b.py
##################################################

import numpy as np
from scipy import special
from sklearn.neighbors import BallTree
import seaborn as sns
import matplotlib.pyplot as plt

points = []
file = 'unknown_xy_data_2017_1.txt'
with open(file) as f:
    for line in f:
        split = list(map(float, line.split()))
        points.append(split)
points = np.array(points)

def n_volume(r, n):
    return np.pi ** (0.5 * n) / special.gamma(0.5 * n + 1) * (r ** n)

class KNeighborsDensity:
    def __init__(self, n_neighbors=10):
        self.n_neighbors = n_neighbors

    def fit(self, X):
        self.X_ = np.atleast_2d(X)
        if self.X_.ndim != 2:
            raise ValueError('X must be two-dimensional')
        self.bt_ = BallTree(self.X_)
        return self

    def eval(self, X):
        X = np.atleast_2d(X)
        if X.ndim != 2:
            raise ValueError('X must be two-dimensional')
        if X.shape[1] != self.X_.shape[1]:
            raise ValueError('dimensions of X do not match training dimension')
        dist, ind = self.bt_.query(X, self.n_neighbors, return_distance=True)
        k = float(self.n_neighbors)
        ndim = X.shape[1]
        return k / n_volume(dist[:, -1], ndim)
        return dens

ks = [1, 2, 4, 8, 16, 32, 64]
for k in ks:
    knd = KNeighborsDensity(k)
    knd.fit(points)
    values = knd.eval(points)
    print(values)
    
    f, ax = plt.subplots(figsize=(6,6))
    cmap = sns.cubehelix_palette(as_cmap=True, dark=0, light=1, reverse=True)
    sns.kdeplot(points, cmap=cmap, n_levels=k, shade=True);
    filename = str(k) + 'kde.png'
    plt.scatter(points[:, 0], points[:, 1], s=.8)
    plt.savefig(filename)
    plt.clf()
