##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 03b (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question1.py
##################################################

import matplotlib.pyplot as plt
import numpy as np
import math
from scipy.stats import multivariate_normal
import seaborn as

file = 'gaussian_wxyz_data_2017_1.txt'
points = []
with open(file) as f:
	for line in f:
		split = list(map(float, line.split()))
		points.append(np.array(split))
points = np.array(points)

def gaussScatterplot(points):
	r = [int(i) for i in range(0, len(points[0]))]
	for i in r:
		for j in r:
			xlabel = "x" + str(i)
			ylabel = "x" + str(j)
			plt.xlabel(xlabel)
			plt.ylabel(ylabel)
			filename = "scatter" + xlabel + ylabel
			plt.scatter(points[:,i], points[:,j])
			plt.savefig(filename)
			plt.clf()

def mle(points):
	mu = points.mean(axis=0)

	subs = [np.outer((point-mu), (point-mu)) for point in points]
	subs = np.array(subs)
	sigma = sum(subs) / len(subs)
	Lambda = np.linalg.inv(sigma)
	return mu, sigma, Lambda

def conditionalProbEstimates(points, M, xB):
	mu, sigma, Lambda = mle(points)
	D = len(mu)

	muA, muB = mu[:M], mu[M:]
	if xB.shape != muB.shape:
		raise Exception("The point has an incorrect shape.")
	sigmaAA, sigmaAB, sigmaBA, sigmaBB = sigma[:M, :M], sigma[:M, M:], sigma[M:, :M], sigma[M:, M:]
	LambdaAA, LambdaAB, LambdaBA, LambdaBB = Lambda[:M, :M], Lambda[:M, M:], Lambda[M:, :M], Lambda[M:, M:]

	muAcondB = muA + np.dot(np.dot(sigmaAB, np.linalg.inv(sigmaBB)), (xB - muB))
	sigmaAcondB = sigmaAA - np.dot(np.dot(sigmaAB, np.linalg.inv(sigmaBB)), sigmaBA)
	LambdaAcondB = np.linalg.inv(sigmaAcondB)
	
	return muAcondB, sigmaAcondB, LambdaAcondB

def multivariateNormalPlot(points, M, mu, sigma):
	pass

gaussScatterplot(points)
print(mle(points))
print(conditionalProbEstimates(points=points, M=2, xB=np.array([0,0])))
print(conditionalProbEstimates(points=points, M=1, xB=np.array([0,0,0])))

