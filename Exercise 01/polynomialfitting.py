#######################################################
# Advanced Topics in Artificial Intelligence (CK0146)
# Exercise 01
# Author: Joaquim Filipe Lobo de Sousa (344066)
# polynomialfitting.py
#######################################################

import numpy as np
import random as rand
import matplotlib.pyplot as plt

def getParams():
	x = np.linspace(0, 1, 100)
	f = 2 * 5 * x ** (2 - 1) * (1 - x ** 2) ** (5 - 1)
	t = np.asarray([number + rand.gauss(0, 0.3) for number in f])
	return x, f, t

def getW(x, t, Lambda, M):
	tx = np.vander(x, M + 1)
	g = Lambda * np.eye(tx.shape[1])
	g[0,0] = 0
	w = np.dot(np.linalg.inv(np.dot(tx.T, tx) + np.dot(g.T, g)), np.dot(tx.T, t))
	y = np.dot(tx, w)
	return w, y

def getError(t, y, w, Lambda):
	error = 0
	for index, value in enumerate(t):
		error += (y[index] - value)**2
	error /= 2
	error += (.5 * Lambda * np.sum(w**2))
	return error

x, f, t = getParams()
w1, y1 = getW(x, t, 0, 4)
w2, y2 = getW(x, t, 0.01, 4)

plt.plot(x, t, 'go', label='Noisy samples')
plt.plot(x, f, 'b', label='Target function')
plt.plot(x, y1, 'r', label='OLS')
plt.plot(x, y2, 'y', label='Ridge (Lambda = 0.01)')

plt.legend()
plt.show()
