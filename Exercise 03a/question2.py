##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 03a (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question2.py
##################################################

import numpy as np
import matplotlib.pyplot as plt
import math

filename = 'location_x1_x2_y_2017.txt'
with open(filename) as f:
	content = f.readlines()
content = [x.strip() for x in content]

x, y = [], []
for idx, line in enumerate(content):
	values = line.split()
	x.append([float(values[0]), float(values[1])])
	y.append(int(values[2]))

N = len(x)

detectedPoints = []
for idx, boolean in enumerate(y):
	if boolean:
		detectedPoints.append(x[idx])
muDetected = len(detectedPoints)/N
detectedXs = [x for [x, y] in detectedPoints]
detectedYs = [y for [x, y] in detectedPoints]

meanX, meanY = np.mean(detectedXs), np.mean(detectedYs)
meanDetect = [meanX, meanY]

varX, varY = np.var(detectedXs), np.var(detectedYs)
varDetect = [varX, varY]
covarDetect = np.multiply(varDetect, np.identity(2))

def distance(xi, x):
	return np.sqrt((xi[0] - x[0])**2 + (xi[1] - x[1])**2)

def mui(distance):
	return np.exp(-distance)

def probability(mui, yi):
	return (mui**yi)*((1-mui)**(1-yi))

def probabilityDx(x, y, guess):
	probabilities = []
	for idx, point in enumerate(x):
		probabilities.append(probability(mui(distance(point, guess)), y[idx]))
	return np.prod(probabilities)

Xs = np.random.multivariate_normal(meanDetect, covarDetect, 250)
meanGuessesX, meanGuessesY = np.mean([x for [x, y] in Xs]), np.mean([y for [x, y] in Xs])
meanGuesses = [meanGuessesX, meanGuessesY]

likelihoodEstimates = []
for guessX in Xs:
	likelihoodEstimates.append(probabilityDx(x, y, guessX))

mle = max(likelihoodEstimates)
positionsMle = [i for i, j in enumerate(likelihoodEstimates) if j == mle]

#
# Not entirely sure about the MAP estimation.
#
def beta(a, b, mu):
	return (math.gamma(a+b)/(math.gamma(a)*math.gamma(b)))*(mu**(a-1))*((1-mu)**(b-1))

mapEstimates = []
for guessX in Xs:
	prob = probabilityDx(x, y, guessX)
	mapEstimates.append(prob * beta(2, 2, prob))

maxAp = max(mapEstimates)
positionsMap = [i for i, j in enumerate(mapEstimates) if j == maxAp]

nd = plt.plot([point[0] for point in x if point not in detectedPoints], [point[1] for point in x if point not in detectedPoints], 'ko')
d = plt.plot(detectedXs, detectedYs, 'ro')
ig = plt.plot(meanGuesses[0], meanGuesses[1], 'cs')
ml = plt.plot(Xs[positionsMle[0]][0], Xs[positionsMle[0]][1], 'g*')
mp = plt.plot(Xs[positionsMap[0]][0], Xs[positionsMap[0]][1], 'r*')
plt.legend(['Non-detecting points', 'Detecting points', 'Initial guess', 'MLE', 'MAP'], loc='best')
plt.show()
