##################################################
# Advanced Topics in Artificial Intelligence
# Exercise 03a (CK0146)
# Author: Joaquim Filipe Lobo de Sousa (344066)
# question1.py
##################################################

import numpy as np
import math
import matplotlib.pyplot as plt

occurrences = [6, 10, 14, 13, 6, 1]
sampleSize = 50
N = 5

# Item 1)
plt.bar(list(range(0, len(occurrences))), [x/sampleSize for x in occurrences])
plt.xlabel('m')
plt.show()

# Item 2)
def choose(N, m):
	return math.factorial(N) / (math.factorial(N - m) * math.factorial(m))

def mle(sampleSize, occurrences, N):
	return sum([(occur*m)/sampleSize for m, occur in enumerate(occurrences)])/N

def binom(N, mu):
	return [choose(N, m)*mu**m*(1-mu)**(N-m) for m in range(0, N + 1)]

maximumLikelihood = mle(sampleSize, occurrences, N)
binomial = binom(N, maximumLikelihood)
plt.bar(list(range(0, len(occurrences))), binomial)
plt.xlabel('m')
plt.show()

# Item 3)
def binomRange(n, N, mu):
	return [choose(N, m)*mu**m*(1-mu)**(N-m) for m in range(n, N + 1)]

print(sum(binomRange(3, 5, maximumLikelihood)))

# Item 4)
def beta(a, b):
	return [(math.gamma(a+b)/(math.gamma(a)*math.gamma(b)))*(mu**(a-1))*((1-mu)**(b-1)) for mu in np.linspace(0, 1, 100)]

plt.plot(np.linspace(0, 1, 100), beta(2, 2))
plt.xlabel('mu')
plt.show()

# Item 5)
def betaPosterior(a, b, N, occurrences):
	occurA, occurB = 0, 0
	for m, occur in enumerate(occurrences):
		for i in range(0, occur):
			l = N - m
			occurA += m
			occurB += l
	a += (occurA/50)
	b += (occurB/50)
	print(a,b)
	plt.plot(np.linspace(0, 1, 100), beta(a, b))
	plt.xlabel('mu')
	plt.show()

betaPosterior(2, 2, N, occurrences)

# Item 6)
def expectedPosterior(a, b, N, occurrences):
	occurA, occurB = 0, 0
	for m, occur in enumerate(occurrences):
		for i in range(0, occur):
			l = N - m
			occurA += m
			occurB += l
	a += (occurA/50)
	b += (occurB/50)
	E = a/(a+b)
	print(E)

expectedPosterior(2, 2, N, occurrences)